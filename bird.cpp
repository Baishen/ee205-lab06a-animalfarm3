///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

   void Bird::printInfo() {
Animal::printInfo();
cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
cout << boolalpha << "   Is Migratory = [" << isMigratory << "]" << endl;

}

const string Bird::speak(){
return string ( "Tweet" );
}

}

