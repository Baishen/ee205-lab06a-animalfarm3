###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################
all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp animal.hpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	
clean:
	rm -f *.o main

