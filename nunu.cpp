///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <string> 
#include <iostream>

#include "nunu.hpp"

using namespace std; 

namespace animalfarm {

Nunu::Nunu( bool isNative, enum Color newColor, enum Gender newGender ) {
gender = newGender;

species = "Fistularia chinensis"; 

scaleColor = newColor;

favoriteTemp = 80.6;

native = isNative;

}

void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << boolalpha << "   Is native = [" << native << "]" << endl;
   Fish::printInfo();

}
   

} // namespace animalfarm

